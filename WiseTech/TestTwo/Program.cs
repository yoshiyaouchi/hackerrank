﻿using System;
using System.Collections.Generic;

namespace TestTwo
{
    class Result
    {
        public static int findLowestPrice(List<List<string>> products, List<List<string>> discounts)
        {
            int total = 0;
            for (int i = 0; i < products.Count; i++)
            {
                double price = Convert.ToDouble(products[i][0]);
                //Console.WriteLine("Original price: " + price);
                for (int j = 1; j < products[i].Count; j++)
                {
                    for (int k = 0; k < discounts.Count; k++)
                    {
                        //Console.WriteLine(products[i][j] + " " + discounts[k][0]);
                        if (products[i][j] == discounts[k][0])
                        {
                            if (discounts[k][1] == "0")
                                price = Convert.ToInt32(discounts[k][2]);
                            else if (discounts[k][1] == "1")
                                price = price - price * (Convert.ToDouble(discounts[k][2]) / 100);
                            else
                                price = price - Convert.ToInt32(discounts[k][2]);
                            //Console.WriteLine("New price: " + price);
                        }
                    }
                }
                total += Convert.ToInt32(price);
                //Console.WriteLine("Total: " + total);
            }
            Console.WriteLine(total);
            return total;
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            List<List<string>> products = new List<List<string>>();
            List<List<string>> discounts = new List<List<string>>();

            List<string> product = new List<string>();
            product.Add("10");
            product.Add("sale");
            product.Add("january-sale");

            List<string> product2 = new List<string>();
            product2.Add("200");
            product2.Add("sale");
            product2.Add("EMPTY");

            products.Add(product);
            products.Add(product2);

            List<string> discount1 = new List<string>();
            discount1.Add("sale");
            discount1.Add("0");
            discount1.Add("10");

            List<string> discount2 = new List<string>();
            discount2.Add("january-sale");
            discount2.Add("1");
            discount2.Add("10");

            discounts.Add(discount1);
            discounts.Add(discount2);

            //nsole.WriteLine(Convert.ToInt32(102.7));

            int answer = Result.findLowestPrice(products, discounts);
        }
    }
}
